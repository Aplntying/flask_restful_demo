
from flask import Flask, Manager, abort
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
app.debug = True
api = Api(app)
manager = Manager(app)

if __name__ == '__main__':
  manager.run()


class UserAPI(Resource):
  def get(self, id):
    pass
  
  def put(self, id):
    pass

  def delete(self, id):
    pass

api.add_resource(UserAPI, '/usrs/<int:id>', endpoint = 'user')


class TaskListAPI(Resource):
  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('title', type = str, rquired = True,
      help = 'No task title provided', location = 'json')
    self.reqparse.add_argument('description', type = str, default = '', 
      location = 'json')
    super(TaskListAPI, self).__init__()

  def get(self):
    pass

  def post(self):
    pass

class TaskAPI(Resource):
  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_resource('title', type = str, location = 'json')
    self.reqparse.add_resource('description', type = str, location = 'json')
    self.reqparse.add_resource('done', type = bool, location = 'json')
    super(TaskAPI, self).__init__()

  def get(self, id):
    pass

  def put(self, id):
    task = filter(lambda t: t['id'] == id ,tasks)
    if len(task) == 0:
      abort(404)
    task = task[0]
    args = self.reqparse.parse_args()
    for k, v in args.iteritems():
      if v != None:
        task[k] = v
    #return jsonify({'task': make_public_task(task)})
    return {'task': make_public_task(task)}, 201


  def delete(self, id):
    pass

api.add_resource(TaskListAPI, '/todo/api/v1.0/tasks', endpoint = 'tasks')
api.add_resource(TaskAPI, '/todo/api/v1.0/tasks/<int:id>', endpoint = 'task')




